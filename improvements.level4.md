# Improvements

## Persistence
By default very few data are persistent. Need to persist data to avoid to loose them if the pod has to restart for any reason. 
For now only Minio and Postgres DB are persist. Need to add persistence for monitoring (ELK and Prometheus) to keep logs preview available even if pods crash.

## RBAC
By default some serviceaccounts are not created. Need to add service account for each tools/apps otherwise it will use the default serviceaccount.

## Automation
Use terraform with kubernetes and helm provider in order to fast deploy the technical stack and store its state with S3 backend

## Security
Credentials were left with their default value. Need to change that to random value or predefined value coming for Vault for example

## Recovery disaster
Better use velero/restic to configure volume backup

## Performance
- Need to change request/limit resources for each pod
- Add HorizontalPodScaling and PoddistruptionBudget
