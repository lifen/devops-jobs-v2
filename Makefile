SUBDIRS = lifen-traefik lifen-minio lifen-sidekiq lifen-elk lifen-prometheus
install_all:
	for dir in $(SUBDIRS); do \
	         $(MAKE) -C $$dir; \
		     done

purge_all:
	for dir in $(SUBDIRS); do \
	         $(MAKE) -C $$dir purge; \
		     done
