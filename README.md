#####

# ONE SHOT
## Requirements
- helm v3

## DEPLOY ALL
To install all:
```sh
export KUBECONFIG=<PATH_TO_KUBECONFIG>
make install_all
```
This will create required namespaced and deploy helm releases in its namepaces:
- traefik (in ingress ns)
- minio (in object-storage ns)
- elk (in monitoring-apps ns )
- prometheus (in monitoring-kube ns )
- sidekiq -- webapps and db (in apps ns)


## REMOVE ALL
To purge all:
```sh
make purge_all
```


# SPECIFIC APP/TOOLS
## DEPLOY
```
cd lifen-<apps>
make install
```

## PURGE
```
cd lifen-<apps>
make purge
```
